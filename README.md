# Project Title

Speakap

## Project Description

Front-end test for Speakap by Illona Fedora. This project is using Vue CLI.

## Usage

Installing dependencies

```
$ npm install
```

Local development

```
$ npm run dev
```

## Deployment

Automatically deployed on git push to master

## Built With

* [Vue.js Templates](https://github.com/vuejs-templates/webpack) - The web framework used
* [VeeValidate](https://github.com/baianat/vee-validate) - Form validation
* [Bootstrap 4](https://getbootstrap.com/docs/4.3/getting-started/introduction/) - The styling grid used
* [VueCarousel](https://github.com/fengyuanchen/vue-carousel) - Carousel for testimonial section
* [Chart.js](hhttps://github.com/chartjs/Chart.js) - Doughnut chart